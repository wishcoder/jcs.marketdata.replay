/**
 * 
 */
package com.wishcoder.samples.jcs.trade.server;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.border.BevelBorder;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import org.apache.log4j.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wishcoder.samples.jcs.trade.common.components.FixedColumnTable;
import com.wishcoder.samples.jcs.trade.common.components.RoundedPanel;
import com.wishcoder.samples.jcs.trade.common.data.Constants;
import com.wishcoder.samples.jcs.trade.common.data.CurrencyPair;
import com.wishcoder.samples.jcs.trade.common.data.LoginRequest;
import com.wishcoder.samples.jcs.trade.common.data.LoginResponse;
import com.wishcoder.samples.jcs.trade.common.data.LogoutRequest;
import com.wishcoder.samples.jcs.trade.common.data.Security;
import com.wishcoder.samples.jcs.trade.common.data.Tick;
import com.wishcoder.samples.jcs.trade.common.data.User;
import com.wishcoder.samples.jcs.trade.common.logger.ILogger;
import com.wishcoder.samples.jcs.trade.common.logger.LoggerAdapter;
import com.wishcoder.samples.jcs.trade.server.table.UserTable;

import net.miginfocom.swing.MigLayout;

/**
 * @author ajaysingh
 *
 */
public class ServerApp extends JFrame implements ILogger {
	private static final long serialVersionUID = 8813502099539434906L;

	protected static final Logger LOG = LoggerFactory.getLogger(ServerApp.class);

	// pale turquoise
	private static final Color COLOR_BACKGROUND_APP = Color.decode("#AFEEEE");
	// sky blue
	private static final Color COLOR_BACKGROUND_BOTTOM_PANEL = Color.decode("#87CEEB");
	// lemon chiffon
	private static final Color COLOR_BACKGROUND_LOG_CONSOLE = Color.decode("#FFFACD");

	private static final SimpleDateFormat dateFormatLogTimestamp = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss:SSS");

	// table panel
	private RoundedPanel userTablePanel;
	private UserTable userTable;
	private JScrollPane userTableScrollPane;
	@SuppressWarnings("unused")
	private FixedColumnTable userTable_fixedColumnTable;

	// Application logs panel
	private static JTextPane logConsoleTextPane;
	private static StyledDocument logConsoleTextPaneStyleDocument;
	private static Style logConsoleTextPaneErrorStyle;
	private static Style logConsoleTextPaneTimestampStyle;
	private JScrollPane logConsoleTextPaneScrollPane;

	// Client/Server
	private final int port;
	private final int holdingTimer;

	private ExecutorService clientExecutorService;
	private ServerSocket serverSocket;
	private final Map<String, Socket> clientSockets;

	private static boolean acceptMore = true;
	private final Map<Integer, Security> securityMap;
	private final ScheduledExecutorService tickerScheduler;

	// User
	private final List<User> userList;
	private boolean allUsersInOfflineMode = false;

	/**
	 * main
	 * 
	 * @param arg
	 * @throws Exception
	 */
	public static void main(String arg[]) throws Exception {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					createAndShowGUI();
				} catch (Exception e) {
					LOG.error(e.getMessage());
				}
			}
		});
	}

	/**
	 * Create the GUI and show it. For thread safety, this method should be
	 * invoked from the event-dispatching thread.
	 */
	private static void createAndShowGUI() throws Exception {
		final ServerApp serverApp = new ServerApp();
		serverApp.setLayout(new MigLayout());
		serverApp.getContentPane()
				.setBackground(Constants.getColor(Constants.app_background_color, COLOR_BACKGROUND_APP));
		serverApp.setTitle(Constants.getProperty(Constants.app_title));

		// size of the screen
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Constants.APP_WIDTH = (int) (screenSize.getWidth() / 2) + 250;
		Constants.APP_HEIGHT = (int) (screenSize.getHeight() / 2) + 150;

		serverApp.setSize(new Dimension(Constants.APP_WIDTH, Constants.APP_HEIGHT));

		serverApp.setLocationRelativeTo(null);
		serverApp.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		serverApp.setIconImage(
				new ImageIcon(ServerApp.class.getResource(Constants.getProperty(Constants.app_icon))).getImage());

		serverApp.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				try {
					serverApp.removeAllClients();
				} catch (Exception e1) {
					LOG.error("Exception while existing trade server application - " + e1.getMessage());
				}
			}
		});

		// add components
		serverApp.add(serverApp.getUserTablePanel(), "span, center,gapbottom 5");
		serverApp.add(serverApp.getLogPanel(), "span, center, gapbottom 0");

		serverApp.setResizable(false);
		serverApp.setVisible(true);
		
		serverApp.getUserTable().resetUserTable(serverApp.getUserList());
		LoggerAdapter.getInstance().initInstance(serverApp);
		TimeUnit.MILLISECONDS.sleep(250);
		serverApp.start();
	}

	/**
	 * getJiraIssueTablePanel
	 * 
	 * @return userTablePanel
	 * @throws Exception
	 */
	public RoundedPanel getUserTablePanel() throws Exception {
		userTablePanel = new RoundedPanel(new MigLayout("fill"));
		userTablePanel.setPreferredSize(new Dimension((Constants.APP_WIDTH - 25), (Constants.APP_HEIGHT - 25)));
		userTablePanel.setBackground(
				Constants.getColor(Constants.app_background_color_bottom_panel, COLOR_BACKGROUND_BOTTOM_PANEL));

		// initialize TeamTrack issues table
		userTable = new UserTable();

		// Create the scroll pane and add the salesWebDataTable to it.
		userTableScrollPane = new JScrollPane(userTable);
		userTableScrollPane.setAlignmentX(Component.CENTER_ALIGNMENT);
		userTableScrollPane.setPreferredSize(new Dimension((Constants.APP_WIDTH - 25), (Constants.APP_HEIGHT - 100)));
		userTable_fixedColumnTable = new FixedColumnTable(1, userTableScrollPane);
		userTablePanel.add(userTableScrollPane, "spanx 3, growx, growy, wrap");
		return userTablePanel;
	}

	/**
	 * getLogPanel
	 * 
	 * @return Log JPanel
	 * @throws Exception
	 */
	private JPanel getLogPanel() throws Exception {
		// LOG view
		logConsoleTextPane = new JTextPane();
		logConsoleTextPaneStyleDocument = logConsoleTextPane.getStyledDocument();
		logConsoleTextPaneErrorStyle = logConsoleTextPane.addStyle("Error", null);
		StyleConstants.setForeground(logConsoleTextPaneErrorStyle, Color.red);
		StyleConstants.setBold(logConsoleTextPaneErrorStyle, true);
		logConsoleTextPaneTimestampStyle = logConsoleTextPane.addStyle("Timestamp", null);
		StyleConstants.setForeground(logConsoleTextPaneTimestampStyle,
				Color.decode(Constants.getProperty(Constants.app_timestamp_color_log_console)));
		StyleConstants.setBold(logConsoleTextPaneTimestampStyle, true);
		logConsoleTextPane.setBackground(
				Constants.getColor(Constants.app_background_color_log_console, COLOR_BACKGROUND_LOG_CONSOLE));
		logConsoleTextPane.setBorder(new BevelBorder(BevelBorder.LOWERED));
		logConsoleTextPane.setEditable(false);
		// Create the scroll pane and add the
		// logConsoleTextPane to it.
		logConsoleTextPaneScrollPane = new JScrollPane(logConsoleTextPane);
		logConsoleTextPaneScrollPane.setAlignmentX(Component.CENTER_ALIGNMENT);
		logConsoleTextPaneScrollPane.setPreferredSize(new Dimension((Constants.APP_WIDTH - 25), 300));
		JPanel logPanel = new JPanel(new MigLayout("insets 0 0 0 0"));
		logPanel.setOpaque(false);
		logPanel.add(logConsoleTextPaneScrollPane);
		return logPanel;
	}

	/**
	 * loadConfiguration
	 */
	private void loadConfiguration() throws Exception {
		String customPropertiesPath = System.getProperty("user.dir") + File.separator + "custom.properties";

		final Properties props = new Properties();
		final InputStream appProperties = ServerApp.class.getResourceAsStream("application.properties");
		InputStream customProperties = null;

		try {
			customProperties = new FileInputStream(customPropertiesPath);
		} catch (Exception e) {
			LOG.error("Failed to load custom.properties");
		}
		try {
			if (appProperties != null) {
				props.load(appProperties);
			}
			if (customProperties != null) {
				props.load(customProperties);
			}

			String key = "";
			String value = "";

			for (final Entry<Object, Object> entry : props.entrySet()) {
				key = entry.getKey().toString().trim().toLowerCase();
				value = entry.getValue().toString().trim();
				System.setProperty(key, value);
			}

			LOG.info("System properties initilized. Custom properties found: "
					+ (customProperties == null ? "NO" : "YES"));

		} catch (IOException e) {
			throw new RuntimeException("Failed to load application.properties due to: " + e.getMessage(), e);
		} catch (Exception e) {
			throw new RuntimeException("Failed to load application.properties due to: " + e.getMessage(), e);
		} finally {
			try {
				if (appProperties != null) {
					appProperties.close();
				}
				if (customProperties != null) {
					customProperties.close();
				}
			} catch (IOException e) {
				// don't care
			}
		}
	}

	/**
	 * insertLog
	 * 
	 * @param logLevel
	 * @param logMessage
	 * @param timeStamp
	 */
	public void insertLog(final Level logLevel, final String logMessage, final long timeStamp) {
		if (logConsoleTextPane != null) {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					try {
						String logTimestampStr = (dateFormatLogTimestamp.format(new Date(timeStamp)) + " - ");

						Style logStyle = null;
						if (logLevel == Level.ERROR) {
							logStyle = logConsoleTextPaneErrorStyle;
						} else {
							if (("" + logMessage).toLowerCase().indexOf("total execution time") != -1
									|| ("" + logMessage).toLowerCase().indexOf("processing (") != -1) {
								logStyle = logConsoleTextPaneTimestampStyle;
							}
						}

						logConsoleTextPaneStyleDocument.insertString(logConsoleTextPaneStyleDocument.getLength(),
								logTimestampStr, logConsoleTextPaneTimestampStyle);

						logConsoleTextPaneStyleDocument.insertString(logConsoleTextPaneStyleDocument.getLength(),
								logMessage + "\n", logStyle);
						logConsoleTextPane.setCaretPosition(logConsoleTextPane.getText().length() - 1);
					} catch (Exception e) {
						// ignore
					}
				}
			});
		}
	}

	/**
	 * 
	 */
	public ServerApp() throws Exception {
		this.port = Integer.getInteger("port", 9090);
		this.holdingTimer = Integer.getInteger("holdingTimer", 250); // milliseconds
																		

		loadConfiguration();

		this.userList = new ArrayList<User>();
		this.clientExecutorService = Executors.newFixedThreadPool(100);
		this.clientSockets = new HashMap<String, Socket>();
		this.tickerScheduler = Executors.newScheduledThreadPool(1);

		securityMap = new HashMap<Integer, Security>();
		securityMap.put(CurrencyPair.AUD_CAD.getId(), new Security(CurrencyPair.AUD_CAD.getId(), CurrencyPair.AUD_CAD));
		securityMap.put(CurrencyPair.AUD_USD.getId(), new Security(CurrencyPair.AUD_USD.getId(), CurrencyPair.AUD_USD));
		securityMap.put(CurrencyPair.EUR_AUD.getId(), new Security(CurrencyPair.EUR_AUD.getId(), CurrencyPair.EUR_AUD));
		securityMap.put(CurrencyPair.EUR_CAD.getId(), new Security(CurrencyPair.EUR_CAD.getId(), CurrencyPair.EUR_CAD));
		securityMap.put(CurrencyPair.EUR_GBP.getId(), new Security(CurrencyPair.EUR_GBP.getId(), CurrencyPair.EUR_GBP));
		securityMap.put(CurrencyPair.CAD_JPY.getId(), new Security(CurrencyPair.CAD_JPY.getId(), CurrencyPair.CAD_JPY));
		securityMap.put(CurrencyPair.CHF_RON.getId(), new Security(CurrencyPair.CHF_RON.getId(), CurrencyPair.CHF_RON));
		securityMap.put(CurrencyPair.EUR_HUF.getId(), new Security(CurrencyPair.EUR_HUF.getId(), CurrencyPair.EUR_HUF));
		securityMap.put(CurrencyPair.EUR_NZD.getId(), new Security(CurrencyPair.EUR_NZD.getId(), CurrencyPair.EUR_NZD));
		securityMap.put(CurrencyPair.GBP_BGN.getId(), new Security(CurrencyPair.GBP_BGN.getId(), CurrencyPair.GBP_BGN));
		securityMap.put(CurrencyPair.HKD_JPY.getId(), new Security(CurrencyPair.HKD_JPY.getId(), CurrencyPair.HKD_JPY));
		securityMap.put(CurrencyPair.USD_CAD.getId(), new Security(CurrencyPair.USD_CAD.getId(), CurrencyPair.USD_CAD));
		securityMap.put(CurrencyPair.USD_JPY.getId(), new Security(CurrencyPair.USD_JPY.getId(), CurrencyPair.USD_JPY));
	}

	/**
	 * @return the userList
	 */
	public List<User> getUserList() {
		return userList;
	}

	/**
	 * @return the userTable
	 */
	public UserTable getUserTable() {
		return userTable;
	}
	
	/**
	 * @return the allUsersInOfflineMode
	 */
	public boolean isAllUsersInOfflineMode() {
		return allUsersInOfflineMode;
	}

	/**
	 * @param allUsersInOfflineMode the allUsersInOfflineMode to set
	 */
	public void setAllUsersInOfflineMode(boolean allUsersInOfflineMode) {
		this.allUsersInOfflineMode = allUsersInOfflineMode;
	}

	/**
	 * Start server
	 * 
	 * @throws Exception
	 */
	public void start() throws Exception {
		try {
			final ServerApp serverApp = this;
			SwingWorker<Void, Void> serverWorker = new SwingWorker<Void, Void>() {
				@Override
				protected Void doInBackground() throws Exception {
					tickerScheduler.schedule(new TickerThread(), holdingTimer, TimeUnit.MILLISECONDS);
					
					serverSocket = new ServerSocket(port);
					
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							LOG.info("Trade server started on port: " + port);
							setTitle(getTitle() + " [ localhost:" + port + " ]");
						}
					});
					
					while (acceptMore) {
						if (!acceptMore)
							break;
						Socket clientSocket = serverSocket.accept();
						clientExecutorService.submit(new ServerSocketCallable(serverApp, clientSocket));
					}
					return null;
				}

				@Override
				public void done() {
					String serverError = "";
					try {
						if(serverSocket != null && !serverSocket.isClosed())
							serverSocket.close();
						
						serverSocket = null;
					} catch (IOException exp) {
						serverError = exp.getMessage();
					} catch (Exception e) {
						serverError = e.getMessage();
					} finally {
						clientExecutorService.shutdownNow();
						if (!"".equals(serverError)) {
							LOG.error("Trade server stopped due to the error: " + serverError);
						} else {
							LOG.info("Trade server stopped.");
						}
						serverApp.dispose();
						System.exit(0);
					}

				}
			};

			serverWorker.execute();
		} catch (Exception exp) {
			LOG.error("Trade server stopped due to the error: " + exp.getMessage());
		}
	}

	/**
	 * addClient
	 * 
	 * @param loginResponse
	 * @param offline
	 * @param clientSocket
	 */
	public void addClient(LoginResponse loginResponse, boolean offline, Socket clientSocket) {
		if (!this.clientSockets.containsKey(loginResponse.getUserId())) {
			this.clientSockets.put(loginResponse.getUserId(), clientSocket);
			final User updatedUser = new User(loginResponse.getUserId(), offline, loginResponse.getName(), loginResponse.getCurrencyPairs());
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					getUserTable().updateUserTable(updatedUser);
					updateAllUsersOfflineMode();
				}
			});
		}
	}

	/**
	 * removeClient
	 * 
	 * @param clientName
	 */
	public void removeClient(String clientName) {
		if (this.clientSockets.containsKey(clientName)) {
			this.clientSockets.remove(clientName);
			final User updatedUser = new User(clientName, false, null, null);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					getUserTable().updateUserTable(updatedUser);
					updateAllUsersOfflineMode();
				}
			});
		}
	}
	
	/**
	 * updateAllUsersOfflineMode
	 */
	public void updateAllUsersOfflineMode(){
		setAllUsersInOfflineMode(true);
		for(User user : getUserList()){
			if(!user.isOffline()){
				setAllUsersInOfflineMode(false);
			}
		}
	}

	/**
	 * removeAllClients
	 */
	public void removeAllClients() {
		if (clientSockets != null && clientSockets.size() > 0) {
			for (Socket clientSocket : clientSockets.values()) {
				if (clientSocket != null && clientSocket.isConnected()) {
					try {
						clientSocket.close();
					} catch (IOException e) {
						LOG.error("Remove all client exception for client - " + clientSocket.toString()
								+ ", Exception - " + e);
					}
					clientSocket = null;
				}
			}
		}
		clientSockets.clear();

		if (serverSocket != null && !serverSocket.isClosed()) {
			try {
				serverSocket.close();
			} catch (IOException e) {
				LOG.error("Remove all client exception for server socket - " + e);
			} finally {
				serverSocket = null;
			}
		}
		
		acceptMore = false;
	}

	class TickerThread implements Runnable {

		@Override
		public void run() {
			try {
				if (!isAllUsersInOfflineMode() && clientSockets != null && clientSockets.size() > 0) {

					Security security = securityMap.get(ThreadLocalRandom.current().nextInt(1, securityMap.size() + 1));
					double bid = ThreadLocalRandom.current().nextDouble(1.00, 100.00);
					double ask = ThreadLocalRandom.current().nextDouble(1.00, 100.00);

					Tick tick = new Tick(security, bid, ask);
					for (Socket clientSocket : clientSockets.values()) {
						ObjectOutputStream oos = new ObjectOutputStream(clientSocket.getOutputStream());
						oos.writeObject(tick);
					}
					
					LOG.info(tick.toString());
				}
			} catch (Exception e) {
				LOG.error("TickerThread exception - " + e);
			} finally {
				tickerScheduler.schedule(new TickerThread(), holdingTimer, TimeUnit.MILLISECONDS);
			}

		}

	}

	/**
	 * ServerSocketCallable
	 * 
	 * @author ajaysingh
	 *
	 */
	static class ServerSocketCallable implements Callable<Object> {
		protected final Logger LOG = LoggerFactory.getLogger(ServerSocketCallable.class);

		private ServerApp serverApp;
		private Socket clientSocket;
		private ObjectInputStream streamIn = null;

		public ServerSocketCallable(ServerApp serverApp, Socket clientSocket) {
			this.serverApp = serverApp;
			this.clientSocket = clientSocket;
		}

		@Override
		public Object call() throws Exception {
			while (true) {
				try {
					if (!open()) {
						return null;
					}

					Object clientRequestObj = streamIn.readObject();
					if (clientRequestObj instanceof LoginRequest) {
						processLoginRequest(clientRequestObj);
					} else if (clientRequestObj instanceof LogoutRequest) {
						processLogoutRequest(clientRequestObj);
					}

				} catch (IOException ioe) {
					LOG.error("Exception in processing client request - " + ioe.getMessage());
					return null;
				}
			}
		}

		/**
		 * open
		 * 
		 * @return boolean
		 * 
		 * @throws IOException
		 */
		private boolean open() throws IOException {
			if (clientSocket != null && !clientSocket.isClosed()) {
				streamIn = new ObjectInputStream(clientSocket.getInputStream());
				return true;
			} else {
				return false;
			}
		}

		/**
		 * processLoginRequest
		 * 
		 * @param clientRequestObj
		 * @throws Exception
		 */
		private void processLoginRequest(Object clientRequestObj) throws Exception {

			LoginRequest loginRequest = (LoginRequest) clientRequestObj;
			LOG.info("Request received from client - " + loginRequest.getUserId());

			List<CurrencyPair> currencyPairs = new ArrayList<CurrencyPair>();

			currencyPairs.add(CurrencyPair.AUD_CAD);
			currencyPairs.add(CurrencyPair.AUD_USD);
			currencyPairs.add(CurrencyPair.EUR_AUD);
			currencyPairs.add(CurrencyPair.EUR_CAD);
			currencyPairs.add(CurrencyPair.EUR_GBP);
			currencyPairs.add(CurrencyPair.CHF_RON);
			currencyPairs.add(CurrencyPair.EUR_HUF);
			currencyPairs.add(CurrencyPair.EUR_NZD);
			currencyPairs.add(CurrencyPair.GBP_BGN);

			String userName = "Little John";
			if ("trader1".equalsIgnoreCase(loginRequest.getUserId())) {
				currencyPairs.add(CurrencyPair.CAD_JPY);
				currencyPairs.add(CurrencyPair.HKD_JPY);
				currencyPairs.add(CurrencyPair.USD_CAD);
				currencyPairs.add(CurrencyPair.USD_JPY);
				userName = "Alan a Dale";
			}

			LoginResponse loginResponse = new LoginResponse(loginRequest.getUserId(), userName, currencyPairs);
			ObjectOutputStream oos = new ObjectOutputStream(clientSocket.getOutputStream());
			oos.writeObject(loginResponse);

			serverApp.addClient(loginResponse, loginRequest.isOffline(), clientSocket);
			LOG.info("Login response sent to client: " + loginResponse.toString());
		}

		/**
		 * close
		 * 
		 * @throws IOException
		 */
		private void processLogoutRequest(Object clientRequestObj) throws IOException, Exception {
			LogoutRequest logoutRequest = (LogoutRequest) clientRequestObj;
			LOG.info("Request received from client: " + logoutRequest.toString());

			serverApp.removeClient(logoutRequest.getUserId());

			if (clientSocket != null)
				clientSocket.close();
			if (streamIn != null)
				streamIn.close();

			LOG.info(logoutRequest.getUserId() + " logged out.");
		}

	}
}
