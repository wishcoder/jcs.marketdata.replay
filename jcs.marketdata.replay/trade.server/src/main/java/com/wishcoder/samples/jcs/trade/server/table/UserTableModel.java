package com.wishcoder.samples.jcs.trade.server.table;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wishcoder.samples.jcs.trade.common.data.User;

/**
 * @author Ajay Singh
 */

public class UserTableModel extends AbstractTableModel {
	private static final long serialVersionUID = 958525772633543159L;

	protected static final Logger LOG = LoggerFactory.getLogger(UserTableModel.class);

	private List<User> userDataList;
	public static String[] columnNames;
	public static Object[][] mColumns;
	static {
		columnNames = new String[User.COL_COUNT];
		for (int colIndex = 0; colIndex < columnNames.length; colIndex++) {
			columnNames[colIndex] = User.getColumnLabelByIndex(colIndex);
		}
		mColumns = new Object[1][columnNames.length];
	}

	public UserTableModel() {

	}

	/**
	 * 
	 * setDataList
	 * 
	 * @param userDataList
	 * 
	 */

	public final void setUserDataList(List<User> dataList) {
		this.userDataList = dataList;
	}

	/**
	 * @return the userDataList
	 */
	public List<User> getUserDataList() {
		return userDataList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	public int getRowCount() {
		return userDataList == null ? 0 : userDataList.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getColumnCount()
	 * 
	 */

	public int getColumnCount() {
		return columnNames.length;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */

	public Object getValueAt(int rowIndex, int columnIndex) {
		User detail = (User) userDataList.get(rowIndex);
		return detail.getColumnValueByIndex(columnIndex);
	}

	/*
	 * Don't need to implement this method unless your table's editable.
	 */

	public boolean isCellEditable(int row, int col) {
		// Note that the data/cell address is constant,
		// no matter where the cell appears onscreen.
		return false;
	}

	/**
	 * getSelectedData
	 *
	 * @param row
	 * @return User
	 * 
	 */
	public User getSelectedData(int row) {
		return (User) userDataList.get(row);
	}

}
