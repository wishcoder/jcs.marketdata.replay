package com.wishcoder.samples.jcs.trade.server.table;

import java.awt.Color;
import java.awt.Component;
import java.util.List;

import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableCellRenderer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wishcoder.samples.jcs.trade.common.data.User;

/**
 * @author Ajay Singh
 */

public class UserTable extends JTable {
	private static final long serialVersionUID = 8066958870978066002L;
	protected static final Border DEFAULT_BORDER = new EmptyBorder(1, 1, 1, 1);
	protected static final Logger LOG = LoggerFactory.getLogger(UserTable.class);

	/**
	 * UserTable
	 */

	public UserTable() {
		super(UserTableModel.mColumns, UserTableModel.columnNames);
		setRowSelectionAllowed(false);
		setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		setDefaultEditor(Object.class, null);

		getColumnModel().getColumn(User.COL_USER_ID).setPreferredWidth(150);
		getColumnModel().getColumn(User.COL_USER_OFFLINE).setPreferredWidth(125);
		getColumnModel().getColumn(User.COL_USER_NAME).setPreferredWidth(150);
		getColumnModel().getColumn(User.COL_USER_SECURITIES).setPreferredWidth(725);
	}

	/**
	 * resetTable
	 * 
	 * @param dataList
	 */
	public void resetUserTable(List<User> dataList) {
		if (dataList != null) {
			setModel(new UserTableModel());
			getUserTableModel().setUserDataList(dataList);
			if (dataList.size() > 0) {
				getUserTableModel().fireTableRowsUpdated(0, dataList.size() - 1);
			} else {
				getUserTableModel().fireTableRowsUpdated(0, 0);
			}
		}
	}

	/**
	 * updateUserTable
	 * 
	 * @param updatedUser
	 */
	public void updateUserTable(User updatedUser) {
		// find User in list
		int currentIndex = -1;
		for (User user : getUserTableModel().getUserDataList()) {
			if (user.getUserId().equalsIgnoreCase(updatedUser.getUserId())) {
				currentIndex = getUserTableModel().getUserDataList().indexOf(user);
				break;
			}
		}
		if (currentIndex != -1) {
			getUserTableModel().getUserDataList().remove(currentIndex);
			getUserTableModel().fireTableDataChanged();
		}else{
			getUserTableModel().getUserDataList().add(updatedUser);
			getUserTableModel().fireTableDataChanged();
		}
	}

	/**
	 * 
	 * getTableModel
	 * 
	 * @return UserTableModel
	 */

	public UserTableModel getUserTableModel() {
		if (getModel() != null && getModel() instanceof UserTableModel) {
			return (UserTableModel) getModel();
		} else {
			return null;
		}
	}

	@Override
	public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
		Component comp = super.prepareRenderer(renderer, row, column);
		// User detail = null;
		// try {
		// detail = ((UserTableModel) getModel()).getSelectedData(row);
		// } catch (Exception e) {
		// // ignore exception
		// }

		//Color bg = (row % 2 == 0 ? Color.CYAN : Color.WHITE);
		comp.setBackground(Color.WHITE);

		return comp;
	}
}
