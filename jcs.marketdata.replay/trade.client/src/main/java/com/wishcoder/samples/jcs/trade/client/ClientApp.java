/**
 * 
 */
package com.wishcoder.samples.jcs.trade.client;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.border.BevelBorder;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import org.apache.jcs.access.CacheAccess;
import org.apache.log4j.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wishcoder.samples.jcs.trade.common.components.RoundedPanel;
import com.wishcoder.samples.jcs.trade.client.table.TickTable;
import com.wishcoder.samples.jcs.trade.common.cache.CacheManager;
import com.wishcoder.samples.jcs.trade.common.components.FixedColumnTable;
import com.wishcoder.samples.jcs.trade.common.data.Constants;
import com.wishcoder.samples.jcs.trade.common.data.CurrencyPair;
import com.wishcoder.samples.jcs.trade.common.data.LoginRequest;
import com.wishcoder.samples.jcs.trade.common.data.LoginResponse;
import com.wishcoder.samples.jcs.trade.common.data.LogoutRequest;
import com.wishcoder.samples.jcs.trade.common.data.Security;
import com.wishcoder.samples.jcs.trade.common.data.Tick;
import com.wishcoder.samples.jcs.trade.common.data.TickBook;
import com.wishcoder.samples.jcs.trade.common.data.TickEvent;
import com.wishcoder.samples.jcs.trade.common.data.User;
import com.wishcoder.samples.jcs.trade.common.logger.ILogger;
import com.wishcoder.samples.jcs.trade.common.logger.LoggerAdapter;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import net.miginfocom.swing.MigLayout;

/**
 * @author ajaysingh
 *
 */
public class ClientApp extends JFrame implements ILogger {
	private static final long serialVersionUID = -6341217338544920046L;

	protected static final Logger LOG = LoggerFactory.getLogger(ClientApp.class);

	// pale turquoise
	private static final Color COLOR_BACKGROUND_APP = Color.decode("#AFEEEE");
	// sky blue
	private static final Color COLOR_BACKGROUND_BOTTOM_PANEL = Color.decode("#87CEEB");
	// lemon chiffon
	private static final Color COLOR_BACKGROUND_LOG_CONSOLE = Color.decode("#FFFACD");

	private static final SimpleDateFormat dateFormatLogTimestamp = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss:SSS");

	// table panel
	private RoundedPanel tickTablePanel;
	private TickTable tickTable;
	private JScrollPane tickTableScrollPane;
	@SuppressWarnings("unused")
	private FixedColumnTable tickTable_fixedColumnTable;

	// Application logs panel
	private static JTextPane logConsoleTextPane;
	private static StyledDocument logConsoleTextPaneStyleDocument;
	private static Style logConsoleTextPaneErrorStyle;
	private static Style logConsoleTextPaneTimestampStyle;
	private JScrollPane logConsoleTextPaneScrollPane;

	// Client/Server
	private final String hostname;
	private final int port;

	private final ExecutorService clientExecutorService;
	private Socket clientSocket;

	// User/TickBook
	private User user;
	private TickBook tickBook;
	private final List<Tick> dataList;

	// Cache
	private final RuntimeMXBean jvmInstance;
	private final long jvmUpTime;
	private final AtomicInteger dataCacheSeq;
	private final CacheManager cacheManager;
	private final int maxObjectsInMemory;
	private final long cacheReplayUpTime;
	private final CacheAccess dataCache;
	private static final String DATA_CACHE_NAME = "TRADE_CACHE";
	private static final String DATA_CACHE_LOCATION = "C:\\TradeCache";
	private static final String DATA_CACHE_REPLAY_MESSAGE = "Application is running in cache replay mode";
	private static final String DATA_CACHE_PERSIST_MESSAGE = "Application is running in cache persistence mode";

	/**
	 * main
	 * 
	 * @param arg
	 * @throws Exception
	 */
	public static void main(String arg[]) throws Exception {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					createAndShowGUI();
				} catch (Exception e) {
					LOG.error(e.getMessage());
				}
			}
		});
	}

	/**
	 * Create the GUI and show it. For thread safety, this method should be
	 * invoked from the event-dispatching thread.
	 */
	private static void createAndShowGUI() throws Exception {
		final ClientApp clientApp = new ClientApp();
		clientApp.setLayout(new MigLayout());
		clientApp.getContentPane()
				.setBackground(Constants.getColor(Constants.app_background_color, COLOR_BACKGROUND_APP));
		clientApp.setTitle(Constants.getProperty(Constants.app_title));

		// size of the screen
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Constants.APP_WIDTH = (int) (screenSize.getWidth() / 2);
		Constants.APP_HEIGHT = (int) (screenSize.getHeight() / 2) + 150;

		clientApp.setSize(new Dimension(Constants.APP_WIDTH, Constants.APP_HEIGHT));

		clientApp.setLocationRelativeTo(null);
		clientApp.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		clientApp.setIconImage(
				new ImageIcon(ClientApp.class.getResource(Constants.getProperty(Constants.app_icon))).getImage());

		clientApp.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				try {
					clientApp.sendLogout();
				} catch (Exception e1) {
					LOG.error("Exception while existing trade client application - " + e1.getMessage());
					e1.printStackTrace();
				} finally {
					LOG.info(
							"Application was running for " + clientApp.getJvmInstance().getUptime() + " milliseconds.");
					e.getWindow().dispose();
					System.exit(0);
				}
			}
		});

		// add components
		clientApp.add(clientApp.getTickTablePanel(), "span, center,gapbottom 5");
		clientApp.add(clientApp.getLogPanel(), "span, center, gapbottom 0");

		clientApp.setResizable(false);
		clientApp.setVisible(true);

		LoggerAdapter.getInstance().initInstance(clientApp);
		TimeUnit.MILLISECONDS.sleep(250);
		clientApp.start();

	}

	/**
	 * getJiraIssueTablePanel
	 * 
	 * @return tickTablePanel
	 * @throws Exception
	 */
	public RoundedPanel getTickTablePanel() throws Exception {
		tickTablePanel = new RoundedPanel(new MigLayout("fill"));
		tickTablePanel.setPreferredSize(new Dimension((Constants.APP_WIDTH - 25), (Constants.APP_HEIGHT - 25)));
		tickTablePanel.setBackground(
				Constants.getColor(Constants.app_background_color_bottom_panel, COLOR_BACKGROUND_BOTTOM_PANEL));

		// initialize TeamTrack issues table
		tickTable = new TickTable();

		// Create the scroll pane and add the salesWebDataTable to it.
		tickTableScrollPane = new JScrollPane(tickTable);
		tickTableScrollPane.setAlignmentX(Component.CENTER_ALIGNMENT);
		tickTableScrollPane.setPreferredSize(new Dimension((Constants.APP_WIDTH - 25), (Constants.APP_HEIGHT - 100)));
		tickTable_fixedColumnTable = new FixedColumnTable(1, tickTableScrollPane);
		tickTablePanel.add(tickTableScrollPane, "spanx 3, growx, growy, wrap");
		return tickTablePanel;
	}

	/**
	 * getLogPanel
	 * 
	 * @return Log JPanel
	 * @throws Exception
	 */
	private JPanel getLogPanel() throws Exception {
		// LOG view
		logConsoleTextPane = new JTextPane();
		logConsoleTextPaneStyleDocument = logConsoleTextPane.getStyledDocument();
		logConsoleTextPaneErrorStyle = logConsoleTextPane.addStyle("Error", null);
		StyleConstants.setForeground(logConsoleTextPaneErrorStyle, Color.red);
		StyleConstants.setBold(logConsoleTextPaneErrorStyle, true);
		logConsoleTextPaneTimestampStyle = logConsoleTextPane.addStyle("Timestamp", null);
		StyleConstants.setForeground(logConsoleTextPaneTimestampStyle,
				Color.decode(Constants.getProperty(Constants.app_timestamp_color_log_console)));
		StyleConstants.setBold(logConsoleTextPaneTimestampStyle, true);
		logConsoleTextPane.setBackground(
				Constants.getColor(Constants.app_background_color_log_console, COLOR_BACKGROUND_LOG_CONSOLE));
		logConsoleTextPane.setBorder(new BevelBorder(BevelBorder.LOWERED));
		logConsoleTextPane.setEditable(false);
		// Create the scroll pane and add the
		// logConsoleTextPane to it.
		logConsoleTextPaneScrollPane = new JScrollPane(logConsoleTextPane);
		logConsoleTextPaneScrollPane.setAlignmentX(Component.CENTER_ALIGNMENT);
		logConsoleTextPaneScrollPane.setPreferredSize(new Dimension((Constants.APP_WIDTH - 25), 300));
		JPanel logPanel = new JPanel(new MigLayout("insets 0 0 0 0"));
		logPanel.setOpaque(false);
		logPanel.add(logConsoleTextPaneScrollPane);
		return logPanel;
	}

	/**
	 * loadConfiguration
	 */
	private void loadConfiguration() throws Exception {
		String customPropertiesPath = System.getProperty("user.dir") + File.separator + "custom.properties";

		final Properties props = new Properties();
		final InputStream appProperties = ClientApp.class.getResourceAsStream("application.properties");
		InputStream customProperties = null;

		try {
			customProperties = new FileInputStream(customPropertiesPath);
		} catch (Exception e) {
			LOG.error("Failed to load custom.properties");
		}
		try {
			if (appProperties != null) {
				props.load(appProperties);
			}
			if (customProperties != null) {
				props.load(customProperties);
			}

			String key = "";
			String value = "";

			for (final Entry<Object, Object> entry : props.entrySet()) {
				key = entry.getKey().toString().trim().toLowerCase();
				value = entry.getValue().toString().trim();
				System.setProperty(key, value);
			}

			// LOG.info("System Propeties:" + System.getProperties());
			LOG.info("System properties initilized. Custom properties found: "
					+ (customProperties == null ? "NO" : "YES"));

		} catch (IOException e) {
			throw new RuntimeException("Failed to load application.properties due to: " + e.getMessage(), e);
		} catch (Exception e) {
			throw new RuntimeException("Failed to load application.properties due to: " + e.getMessage(), e);
		} finally {
			try {
				if (appProperties != null) {
					appProperties.close();
				}
				if (customProperties != null) {
					customProperties.close();
				}
			} catch (IOException e) {
				// don't care
			}
		}
	}

	/**
	 * insertLog
	 * 
	 * @param logLevel
	 * @param logMessage
	 * @param timeStamp
	 */
	public void insertLog(final Level logLevel, final String logMessage, final long timeStamp) {
		if (logConsoleTextPane != null) {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					try {
						String logTimestampStr = (dateFormatLogTimestamp.format(new Date(timeStamp)) + " - ");

						Style logStyle = null;
						if (logLevel == Level.ERROR) {
							logStyle = logConsoleTextPaneErrorStyle;
						} else {
							if (("" + logMessage).toLowerCase().indexOf("total execution time") != -1
									|| ("" + logMessage).toLowerCase().indexOf("processing (") != -1) {
								logStyle = logConsoleTextPaneTimestampStyle;
							}
						}

						logConsoleTextPaneStyleDocument.insertString(logConsoleTextPaneStyleDocument.getLength(),
								logTimestampStr, logConsoleTextPaneTimestampStyle);

						logConsoleTextPaneStyleDocument.insertString(logConsoleTextPaneStyleDocument.getLength(),
								logMessage + "\n", logStyle);
						logConsoleTextPane.setCaretPosition(logConsoleTextPane.getText().length() - 1);
					} catch (Exception e) {
						// ignore
					}
				}
			});
		}
	}

	/**
	 * 
	 */
	public ClientApp() throws Exception {
		this.jvmInstance = ManagementFactory.getRuntimeMXBean();
		this.jvmUpTime = this.jvmInstance.getUptime();
		this.hostname = "localhost";
		this.port = Integer.getInteger("port", 9090);
		this.maxObjectsInMemory = Integer.getInteger("maxObjectsInMemory", 10);
		this.cacheReplayUpTime = Long.getLong("cacheReplayUpTime", 0);

		loadConfiguration();

		this.cacheManager = CacheManager.getInstance();

		if (!getCacheManager().isDataReplayMode() && !getCacheManager().isDataPersistanceMode()) {
			this.dataCache = null;
			this.dataCacheSeq = null;
		} else {
			this.dataCacheSeq = new AtomicInteger(1);
			this.dataCache = getCacheManager().getDataCache(DATA_CACHE_NAME, DATA_CACHE_LOCATION,
					getMaxObjectsInMemory());
		}

		if (getCacheManager().isDataReplayMode()) {
			prepareNonSerializeSetters();
		}

		this.dataList = new ArrayList<Tick>();
		this.clientExecutorService = Executors.newFixedThreadPool(1);

		if (getCacheManager().isDataReplayMode()) {
			LOG.info(DATA_CACHE_REPLAY_MESSAGE);
		} else if (getCacheManager().isDataPersistanceMode()) {
			LOG.info(DATA_CACHE_PERSIST_MESSAGE);
		}
	}

	/**
	 * @return the cacheManager
	 */
	public CacheManager getCacheManager() {
		return cacheManager;
	}

	/**
	 * @return the dataCache
	 */
	public CacheAccess getDataCache() {
		return dataCache;
	}

	/**
	 * @return the dataCacheSeq
	 */
	public AtomicInteger getDataCacheSeq() {
		return dataCacheSeq;
	}

	/**
	 * @return the jvmInstance
	 */
	public RuntimeMXBean getJvmInstance() {
		return jvmInstance;
	}

	/**
	 * @return the jvmUpTime
	 */
	public long getJvmUpTime() {
		return jvmUpTime;
	}

	/**
	 * @return the maxObjectsInMemory
	 */
	public int getMaxObjectsInMemory() {
		return maxObjectsInMemory;
	}

	/**
	 * @return the cacheReplayUpTime
	 */
	public long getCacheReplayUpTime() {
		return cacheReplayUpTime;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @return the dataList
	 */
	public List<Tick> getDataList() {
		return dataList;
	}

	/**
	 * @return the tickTable
	 */
	public TickTable getTickTable() {
		return tickTable;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the tickBook
	 */
	public TickBook getTickBook() {
		return tickBook;
	}

	/**
	 * @param tickBook
	 *            the tickBook to set
	 */
	public void setTickBook(TickBook tickBook) {
		this.tickBook = tickBook;
	}

	public void start() throws Exception {
		try {
			// trying to establish connection to the server
			connect();
		} catch (UnknownHostException e) {
			LOG.error("Host unknown. Cannot establish connection");
		} catch (IOException e) {
			LOG.error("Cannot establish connection. MarketplaceServerApp may not be up." + e.getMessage());
		}
	}

	/**
	 * prepareNonSerializeSetters
	 * 
	 * @throws Exception
	 */
	private void prepareNonSerializeSetters() throws Exception {
		// first instrument TickEvent
		// TickEvent have two non-serializable objects: User and TickBook
		List<CtMethod> setterMethods = new ArrayList<CtMethod>();
		CtClass sourceClass = ClassPool.getDefault().get("com.wishcoder.samples.jcs.trade.common.data.TickEvent");
		CtMethod setSourceMethod = getCacheManager().getMethod(sourceClass, "setSource", "source",
				"com.wishcoder.samples.jcs.trade.common.data.User");
		CtMethod setTickBookMethod = getCacheManager().getMethod(sourceClass, "setTickBook", "tickBook",
				"com.wishcoder.samples.jcs.trade.common.data.TickBook");
		setterMethods.add(setSourceMethod);
		setterMethods.add(setTickBookMethod);
		getCacheManager().addSetterMethods(sourceClass, setterMethods);

		// now store setter parameters in CacheManager for later use
		Class<?>[] userObject = new Class[1];
		userObject[0] = Class.forName("com.wishcoder.samples.jcs.trade.common.data.User");
		Class<?>[] tickBookObject = new Class[1];
		tickBookObject[0] = Class.forName("com.wishcoder.samples.jcs.trade.common.data.TickBook");
		HashMap<String, Class<?>[]> parameterMap = new HashMap<String, Class<?>[]>();
		parameterMap.put("setSource", userObject);
		parameterMap.put("setTickBook", tickBookObject);
		getCacheManager().prepareNonSerializeSetters("com.wishcoder.samples.jcs.trade.common.data.TickEvent",
				parameterMap);
	}

	/**
	 * connect
	 * 
	 * @throws UnknownHostException
	 * @throws IOException
	 * @throws Exception
	 */
	private void connect() throws UnknownHostException, IOException, Exception {
		LOG.info("Attempting to connect to ServerApp @ " + hostname + ":" + port);
		clientSocket = new Socket(hostname, port);
		clientExecutorService.submit(new ClientSocketCallable(this, clientSocket));
		LOG.info("Connection Established on port: " + port);

		// send login
		sendLogin();
	}

	/**
	 * sendLogin
	 * 
	 * @throws Exception
	 */
	private void sendLogin() throws Exception {
		String userId = System.getProperty("userId", "");
		String userPwd = System.getProperty("userPwd", "");

		LoginRequest loginRequest = new LoginRequest(userId, userPwd, getCacheManager().isDataReplayMode());
		ObjectOutputStream oos = new ObjectOutputStream(clientSocket.getOutputStream());
		oos.writeObject(loginRequest);

		LOG.info("Login request sent for user - " + loginRequest.getUserId());
	}

	/**
	 * sendLogout
	 * 
	 * @throws Exception
	 */
	public void sendLogout() throws Exception {
		LOG.info("Exiting application.");

		if (clientSocket == null || clientSocket.isClosed())
			return;

		String userId = System.getProperty("userId", "");

		LogoutRequest logoutRequest = new LogoutRequest(userId);
		ObjectOutputStream oos = new ObjectOutputStream(clientSocket.getOutputStream());
		if (oos != null) {
			try {
				oos.writeObject(logoutRequest);
			} catch (SocketException se) {
				LOG.error("Server connection error - " + se.getMessage());
			}
		}

		clientSocket.close();
		clientSocket = null;

		if (getCacheManager().isDataPersistanceMode()) {
			getCacheManager().shutdownCache(LOG);
		}

		LOG.info("Logout request sent for user - " + logoutRequest.getUserId());

	}

	/**
	 * ClientSocketCallable
	 * 
	 * @author ajaysingh
	 * 
	 */
	static class ClientSocketCallable implements Callable<Object> {
		protected final Logger LOG = LoggerFactory.getLogger(ClientSocketCallable.class);

		private final ClientApp clientApp;
		private final Socket clientSocket;
		private ObjectInputStream streamIn = null;

		ClientSocketCallable(ClientApp clientApp, Socket clientSocket) {
			this.clientApp = clientApp;
			this.clientSocket = clientSocket;
		}

		@Override
		public Object call() throws Exception {
			while (true) {
				try {
					open();

					if (clientSocket != null && !clientSocket.isClosed()) {
						Object serverResponseObj = streamIn.readObject();
						if (serverResponseObj instanceof LoginResponse) {
							handleLoginResponse((LoginResponse) serverResponseObj);
						} else if (serverResponseObj instanceof Tick) {
							if (!clientApp.getCacheManager().isDataReplayMode()) {
								handleTickChange((Tick) serverResponseObj);
							}
						}
					}
				} catch (IOException ioe) {
					LOG.error("Exception in processing server response - " + ioe.getMessage());
					return null;
				}
			}
		}

		/**
		 * handleTickChange
		 * 
		 * @param tick
		 */
		private void handleTickChange(final Tick tick) {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					try {
						TickBook ticBook = new TickBook(clientApp.getUser().getUserId());
						TickEvent tickEvent = new TickEvent(clientApp.getUser(), ticBook, tick);

						// update cache if requested
						if (clientApp.getCacheManager().isDataPersistanceMode()) {
							long timeSeq = clientApp.getJvmInstance().getUptime();
							clientApp.getCacheManager().writeToCache(DATA_CACHE_NAME, timeSeq,
									new Object[] { clientApp.getDataCacheSeq().getAndAdd(1), tickEvent });
						}

						clientApp.getTickTable().updateTickTable(tickEvent);
					} catch (Exception e) {
						LOG.error("Exception in processing tick change - " + e.getMessage());
					}
				}
			});
		}

		/**
		 * handleLoginResponse
		 * 
		 * @param response
		 */
		private void handleLoginResponse(LoginResponse response) {
			final User user = new User(response.getUserId(), true, response.getName(), response.getCurrencyPairs());

			this.clientApp.setUser(user);
			this.clientApp.setTickBook(new TickBook(user.getUserId()));

			LOG.info("User logged in - " + response.toString());

			// now initialize Tick table
			for (CurrencyPair currencyPair : user.getCurrencyPairs()) {
				Tick tick = new Tick(new Security(currencyPair.getId(), currencyPair), 0.0d, 0.0d);
				this.clientApp.getDataList().add(tick);
			}

			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					try {
						String cacheModeMessage = "";
						if (clientApp.getCacheManager().isDataReplayMode()) {
							cacheModeMessage = " [" + DATA_CACHE_REPLAY_MESSAGE + "]";
						} else if (clientApp.getCacheManager().isDataPersistanceMode()) {
							cacheModeMessage = " [" + DATA_CACHE_PERSIST_MESSAGE + "]";
						}

						clientApp.setTitle(clientApp.getTitle() + " [" + user.getName() + "]" + cacheModeMessage);
						clientApp.getTickTable().resetTickTable(clientApp.getDataList());
					} catch (Exception e) {
						LOG.error("Exception in processing initial securities load after login - " + e.getMessage());
					}
				}
			});

			if (this.clientApp.getCacheManager().isDataReplayMode() && this.clientApp.getDataCache() != null) {
				
				long elementCacheSeq = 0;
				long numElementsReplayed = 0;
				long numCachedElements = clientApp.getCacheManager().getStartupSize(DATA_CACHE_NAME);
				long cacheReplayUpTime = this.clientApp.getCacheReplayUpTime();
				
				if (cacheReplayUpTime == 0) {
					// play all messages without delay
					cacheReplayUpTime = 100000000L;
				} else {
					// play all messages based on original JVM sequence
				}
				
				for (long numCurrElementSeq = 0; numCurrElementSeq <= cacheReplayUpTime; numCurrElementSeq++) {
					if (cacheReplayUpTime != 100000000L) {
						try {
							//  play all messages based on original JVM sequence
							TimeUnit.MILLISECONDS.sleep(1);
						} catch (InterruptedException e) {
							// don't log
						}
					}
					
					if(elementCacheSeq >= numCachedElements){
						break;
					}
					
					Object[] cachedObjectArray = (Object[]) this.clientApp.getDataCache().get(numCurrElementSeq);
					if (cachedObjectArray != null && cachedObjectArray.length == 2
							&& cachedObjectArray[1] instanceof TickEvent) {
						
						elementCacheSeq ++;
						
						// enrich cachedObject with User and TickBook
						Object cachedObject;
						try {
							cachedObject = this.clientApp.getCacheManager().setNonSerializeSetter(cachedObjectArray[1],
									"setSource", this.clientApp.getUser());
							cachedObject = this.clientApp.getCacheManager().setNonSerializeSetter(cachedObjectArray[1],
									"setTickBook", this.clientApp.getTickBook());
							this.clientApp.getTickTable().updateTickTable((TickEvent) cachedObject);
							
							numElementsReplayed++;
							LOG.info("CacheManager is replaying cache sequence " + numElementsReplayed + " of total "
									+ numCachedElements + " cached elements.");
							
						} catch (Exception e) {
							LOG.error("Unable to update trade view for cache sequence " + elementCacheSeq
									+ " and cache key " + numCurrElementSeq + ". Cache replay is unable to set User and/or TickBook values for TickEvent.");
						}
					}
				}

				LOG.info("*****************");
				LOG.info("CacheManager successfully replayed " + numElementsReplayed + " elements from total "
						+ numCachedElements + " cached elements.");
				LOG.info("*****************");
			}
		}

		/**
		 * open
		 * 
		 * @throws IOException
		 */
		private void open() throws IOException {
			streamIn = new ObjectInputStream(clientSocket.getInputStream());
		}
	}
}
