package com.wishcoder.samples.jcs.trade.client.table;

import java.awt.Color;
import java.awt.Component;
import java.util.List;

import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableCellRenderer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wishcoder.samples.jcs.trade.common.data.Tick;
import com.wishcoder.samples.jcs.trade.common.data.TickEvent;

/**
 * @author Ajay Singh
 */

public class TickTable extends JTable {
	private static final long serialVersionUID = 2895214759014253257L;
	protected static final Border DEFAULT_BORDER = new EmptyBorder(1, 1, 1, 1);
	protected static final Logger LOG = LoggerFactory.getLogger(TickTable.class);

	/**
	 * TickTable
	 */

	public TickTable() {
		super(TickTableModel.mColumns, TickTableModel.columnNames);
		setRowSelectionAllowed(false);
		setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		setDefaultEditor(Object.class, null);

		getColumnModel().getColumn(Tick.COL_SECURITY).setPreferredWidth(325);
		getColumnModel().getColumn(Tick.COL_BID).setPreferredWidth(150);
		getColumnModel().getColumn(Tick.COL_ASK).setPreferredWidth(150);
	}

	/**
	 * resetTable
	 * 
	 * @param dataList
	 */
	public void resetTickTable(List<Tick> dataList) {
		if (dataList != null) {
			setModel(new TickTableModel());
			getTickTableModel().setTickDataList(dataList);
			if (dataList.size() > 0) {
				getTickTableModel().fireTableRowsUpdated(0, dataList.size() - 1);
			} else {
				getTickTableModel().fireTableRowsUpdated(0, 0);
			}
		}
	}

	/**
	 * updateTickTable
	 * 
	 * @param updatedTickEvent
	 */
	public void updateTickTable(TickEvent updatedTickEvent) {
		if(updatedTickEvent.getUser() == null || updatedTickEvent.getTickBook() == null){
			LOG.error("Unable to update trade view. TickEvent is missing required User and TickBook values.");
			throw new RuntimeException("TickEvent is missing required User and TickBook values.");
		}
		// find security in list
		Tick updatedTick = updatedTickEvent.getTick();

		int currentIndex = -1;
		for (Tick tick : getTickTableModel().getTickDataList()) {
			if (tick.getSecurity().getId() == updatedTick.getSecurity().getId()) {
				currentIndex = getTickTableModel().getTickDataList().indexOf(tick);
				break;
			}
		}
		if (currentIndex != -1) {
			getTickTableModel().getTickDataList().set(currentIndex, updatedTick);
			getTickTableModel().fireTableRowsUpdated(currentIndex, currentIndex);
		}
	}

	/**
	 * 
	 * getTableModel
	 * 
	 * @return TickTableModel
	 */

	public TickTableModel getTickTableModel() {
		if (getModel() != null && getModel() instanceof TickTableModel) {
			return (TickTableModel) getModel();
		} else {
			return null;
		}
	}

	@Override
	public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
		Component comp = super.prepareRenderer(renderer, row, column);
//		Tick detail = null;
//		try {
//			detail = ((TickTableModel) getModel()).getSelectedData(row);
//		} catch (Exception e) {
//			// ignore exception
//		}

		Color bg = (row % 2 == 0 ? Color.CYAN : Color.WHITE);
		comp.setBackground(bg);

		return comp;
	}
}
