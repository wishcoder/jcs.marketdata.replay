package com.wishcoder.samples.jcs.trade.client.table;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wishcoder.samples.jcs.trade.common.data.Tick;

/**
 * @author Ajay Singh
 */

public class TickTableModel extends AbstractTableModel {
	private static final long serialVersionUID = -5608388290089121838L;

	protected static final Logger LOG = LoggerFactory.getLogger(TickTableModel.class);

	private List<Tick> tickDataList;
	public static String[] columnNames;
	public static Object[][] mColumns;
	static {
		columnNames = new String[Tick.COL_COUNT];
		for (int colIndex = 0; colIndex < columnNames.length; colIndex++) {
			columnNames[colIndex] = Tick.getColumnLabelByIndex(colIndex);
		}
		mColumns = new Object[1][columnNames.length];
	}

	public TickTableModel() {

	}

	/**
	 * 
	 * setDataList
	 * 
	 * @param tickDataList
	 * 
	 */

	public final void setTickDataList(List<Tick> dataList) {
		this.tickDataList = dataList;
	}

	/**
	 * @return the tickDataList
	 */
	public List<Tick> getTickDataList() {
		return tickDataList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	public int getRowCount() {
		return tickDataList == null ? 0 : tickDataList.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getColumnCount()
	 * 
	 */

	public int getColumnCount() {
		return columnNames.length;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */

	public Object getValueAt(int rowIndex, int columnIndex) {
		Tick detail = (Tick) tickDataList.get(rowIndex);
		return detail.getColumnValueByIndex(columnIndex);
	}

	/*
	 * Don't need to implement this method unless your table's editable.
	 */

	public boolean isCellEditable(int row, int col) {
		// Note that the data/cell address is constant,
		// no matter where the cell appears onscreen.
		return false;
	}

	/**
	 * getSelectedData
	 *
	 * @param row
	 * @return Tick
	 * 
	 */
	public Tick getSelectedData(int row) {
		return (Tick) tickDataList.get(row);
	}

}
