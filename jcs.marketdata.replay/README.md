# README #

### What is this repository for? ###

* Sample application to show how to persist application data to disk store and replay data from disk store using Apache Java Caching System (JCS)

* Version 1.0

### How do I get set up? ###
 
* Download sample server application from repository: https://bitbucket.org/wishcoder/us.wishcoder.marketplace.server *  

* Download sample client application from repository: https://bitbucket.org/wishcoder/us.wishcoder.marketplace.server*  

* After importing server and client projects run pom.xml in projects root

### Contribution guidelines ###


### Who do I talk to? ###
* View [Data Backup and Replay](https://bitbucket.org/wishcoder/data-backup-and-replay/wiki/Home) wiki for more details
* Ajay Singh [message4ajay@gmail.com]