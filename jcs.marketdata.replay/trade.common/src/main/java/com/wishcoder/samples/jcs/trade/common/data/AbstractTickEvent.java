/**
 * 
 */
package com.wishcoder.samples.jcs.trade.common.data;

import java.util.EventObject;

/**
 * @author ajaysingh
 *
 */
public class AbstractTickEvent extends EventObject {
	private static final long serialVersionUID = 4305187388474628560L;
	protected transient TickBook tickBook;
	
	/**
	 * @param source
	 */
	public AbstractTickEvent(User user, TickBook tickBook) {
		super(user);
		this.tickBook = tickBook;
	}

	/**
	 * @return the tickBook
	 */
	public TickBook getTickBook() {
		return tickBook;
	}

	/**
	 * @return the User
	 */
	public User getUser(){
		return (User) super.getSource();
	}
}
