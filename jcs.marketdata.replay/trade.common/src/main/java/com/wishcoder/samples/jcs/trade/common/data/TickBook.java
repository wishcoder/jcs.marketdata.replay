/**
 * 
 */
package com.wishcoder.samples.jcs.trade.common.data;


/**
 * @author ajaysingh
 *
 */
public class TickBook {

	private final String bookOwner;

	/**
	 * @param bookOwner
	 */
	public TickBook(String bookOwner) {
		this.bookOwner = bookOwner;
	}

	/**
	 * @return the bookOwner
	 */
	public String getBookOwner() {
		return bookOwner;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TickBook [bookOwner=" + bookOwner + "]";
	}
}
