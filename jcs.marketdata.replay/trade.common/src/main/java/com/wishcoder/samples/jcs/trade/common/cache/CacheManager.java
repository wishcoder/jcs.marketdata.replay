/**
 * 
 */
package com.wishcoder.samples.jcs.trade.common.cache;

import java.io.IOException;
import java.lang.instrument.IllegalClassFormatException;
import java.util.HashMap;
import java.util.List;

import org.apache.jcs.access.CacheAccess;
import org.apache.jcs.access.exception.CacheException;
import org.slf4j.Logger;

import javassist.CannotCompileException;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.NotFoundException;

/**
 * @author ajaysingh
 *
 */
public class CacheManager {
	private static CacheManager INSTANCE;
	private final boolean dataPersistanceMode;
	private final boolean dataReplayMode;

	private final CacheReplayHelper cacheReplayHelper;
	private final DiskCacheHelper diskCacheHelper;

	/**
	 * Private constructor
	 */
	private CacheManager() {
		dataReplayMode = Boolean.getBoolean("dataReplayMode");
		if (dataReplayMode) {
			dataPersistanceMode = false;
		} else {
			dataPersistanceMode = Boolean.getBoolean("dataPersistanceMode");
		}

		cacheReplayHelper = new CacheReplayHelper();
		diskCacheHelper = new DiskCacheHelper();
	}

	/**
	 * getInstance
	 * 
	 * @return singleton instance of CacheManager
	 */
	public static CacheManager getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new CacheManager();
		}
		return INSTANCE;
	}

	/**
	 * @return the dataPersistanceMode
	 */
	public boolean isDataPersistanceMode() {
		return dataPersistanceMode;
	}

	/**
	 * @return the dataReplayMode
	 */
	public boolean isDataReplayMode() {
		return dataReplayMode;
	}

	/**
	 * getMethod
	 * 
	 * @param sourceClass
	 * @param setterName
	 * @param setterField
	 * @param setterFieldQualifiedName
	 * @return CtMethod
	 * @throws IllegalClassFormatException
	 * @throws NotFoundException
	 * @throws CannotCompileException
	 * @throws IOException
	 */
	public CtMethod getMethod(CtClass sourceClass, String setterName, String setterField,
			String setterFieldQualifiedName)
			throws IllegalClassFormatException, NotFoundException, CannotCompileException, IOException {
		return cacheReplayHelper.getMethod(sourceClass, setterName, setterField, setterFieldQualifiedName);
	}

	/**
	 * addSetterMethods
	 * 
	 * @param sourceClass
	 * @param methodList
	 * @return Class
	 * @throws IllegalClassFormatException
	 * @throws NotFoundException
	 * @throws CannotCompileException
	 * @throws IOException
	 */
	public Class<?> addSetterMethods(CtClass sourceClass, List<CtMethod> methodList)
			throws IllegalClassFormatException, NotFoundException, CannotCompileException, IOException {
		return cacheReplayHelper.addSetterMethods(sourceClass, methodList);
	}

	/**
	 * prepareForDataReplay
	 * 
	 * @param classKey
	 * @param parameterMap
	 */
	public void prepareNonSerializeSetters(String classKey, HashMap<String, Class<?>[]> parameterMap) {
		cacheReplayHelper.prepareNonSerializeSetters(classKey, parameterMap);
	}

	/**
	 * setNonSerializeSetter
	 * 
	 * @param cachedObject
	 * @param setterName
	 * @param setterParam
	 * @return Object
	 * @throws Exception
	 */
	public Object setNonSerializeSetter(Object cachedObject, String setterName, Object setterParam) throws Exception {
		return cacheReplayHelper.setNonSerializeSetter(cachedObject, setterName, setterParam);
	}

	/**
	 * getDataCache
	 * 
	 * @param cacheName
	 * @param cacheLocation
	 * @param maxObjectInMemory
	 * @return CacheAccess
	 * @throws Exception
	 */
	public CacheAccess getDataCache(String cacheName, String cacheLocation, int maxObjectInMemory) throws Exception {
		return diskCacheHelper.getDataCache(cacheName, cacheLocation, maxObjectInMemory);
	}

	/**
	 * getStartupSize
	 * 
	 * @param cacheName
	 * @return long
	 */
	public long getStartupSize(String cacheName) {
		return diskCacheHelper.getStartupSize(cacheName);
	}

	/**
	 * getKeyMapSize
	 * 
	 * @param cacheName
	 * @return long
	 */
	public long getKeyMapSize(String cacheName) {
		return diskCacheHelper.getKeyMapSize(cacheName);
	}

	/**
	 * writeToCache
	 * 
	 * @param cacheName
	 * @param cacheSeq
	 * @param cacheObject
	 * @throws CacheException
	 */
	public void writeToCache(String cacheName, long cacheSeq, Object[] cacheObject) throws Exception {
		diskCacheHelper.writeToCache(cacheName, cacheSeq, cacheObject);
	}

	/**
	 * shutdownCache
	 */
	public void shutdownCache(Logger LOG) {
		diskCacheHelper.shutdownCache(LOG);
	}
}
