/**
 * 
 */
package com.wishcoder.samples.jcs.trade.common.cache;

import java.io.IOException;
import java.lang.instrument.IllegalClassFormatException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javassist.CannotCompileException;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.CtNewMethod;
import javassist.NotFoundException;

/**
 * @author ajaysingh
 *
 */
class CacheReplayHelper {
	private static final Map<String, HashMap<String, Class<?>[]>> nonSerializeSettersMap = new HashMap<String, HashMap<String, Class<?>[]>>();

	/**
	 * 
	 */
	public CacheReplayHelper() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * addSetterMethods
	 * 
	 * @param sourceClass
	 * @param methodList
	 * @return Class
	 * @throws IllegalClassFormatException
	 * @throws NotFoundException
	 * @throws CannotCompileException
	 * @throws IOException
	 */
	public Class<?> addSetterMethods(CtClass sourceClass, List<CtMethod> methodList)
			throws IllegalClassFormatException, NotFoundException, CannotCompileException, IOException {
		for (CtMethod method : methodList) {
			sourceClass.addMethod(method);
		}
		return sourceClass.toClass();
	}

	/**
	 * getMethod
	 * 
	 * @param sourceClass
	 * @param setterName
	 * @param setterField
	 * @param setterFieldQualifiedName
	 * @return CtMethod
	 * @throws IllegalClassFormatException
	 * @throws NotFoundException
	 * @throws CannotCompileException
	 * @throws IOException
	 */
	public CtMethod getMethod(CtClass sourceClass, String setterName, String setterField,
			String setterFieldQualifiedName)
			throws IllegalClassFormatException, NotFoundException, CannotCompileException, IOException {
		String methodDef = "public void " + setterName + "(" + setterFieldQualifiedName + " _source) { this."
				+ setterField + " = _source; }";
		CtMethod setterMethod = CtNewMethod.make((String) methodDef, (CtClass) sourceClass);
		return setterMethod;
	}

	/**
	 * setSource
	 * 
	 * @param sourceClass
	 * @param paramObject
	 * @param setterName
	 * @param sourceParam
	 * @param eventObject
	 * @return Object
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public Object setSource(Class sourceClass, Class[] paramObject, String setterName, Object sourceParam,
			Object eventObject) throws Exception {
		@SuppressWarnings("unchecked")
		Method method = sourceClass.getDeclaredMethod(setterName, paramObject);
		method.invoke(eventObject, sourceParam);
		return eventObject;
	}

	/**
	 * prepareNonSerializeSetters
	 */
	public void prepareNonSerializeSetters(String classKey, HashMap<String, Class<?>[]> parameterMap) {
		nonSerializeSettersMap.put(classKey, parameterMap);
	}

	/**
	 * setNonSerializeSetter
	 * 
	 * @param eventObject
	 * @param setterName
	 * @param setterParam
	 * @return Object
	 * @throws Exception
	 */
	public Object setNonSerializeSetter(Object cachedObject,String setterName, Object setterParam) throws Exception {
		String classKey = cachedObject.getClass().getName();
		HashMap<String, Class<?>[]> parameterMap = nonSerializeSettersMap.get(classKey);
		if(parameterMap != null){
			Class<?>[] paramObject = parameterMap.get(setterName);
			if(paramObject != null){
				Class<?> sourceClass = Class.forName(classKey);
				return setSource(sourceClass, paramObject, setterName, setterParam, cachedObject);
			}
		}
		return null;
	}
}
