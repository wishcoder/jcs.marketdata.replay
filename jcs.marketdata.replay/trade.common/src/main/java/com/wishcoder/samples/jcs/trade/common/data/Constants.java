package com.wishcoder.samples.jcs.trade.common.data;

import java.awt.Color;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author ajaysingh
 * 
 */
public class Constants {
	protected static final Logger LOG = LoggerFactory.getLogger(Constants.class);

	public static int APP_WIDTH;
	public static int APP_HEIGHT;

	// app view
	public static final String app_title = "app_title";
	public static final String app_icon = "app_icon";

	// app colors
	public static final String app_background_color = "app_background_color";
	public static final String app_background_color_top_panel = "app_background_color_top_panel";
	public static final String app_foreground_color_top_panel = "app_foreground_color_top_panel";
	public static final String app_background_color_bottom_panel = "app_background_color_bottom_panel";
	public static final String app_foreground_color_bottom_panel = "app_foreground_color_bottom_panel";
	public static final String app_background_color_log_console = "app_background_color_log_console";
	public static final String app_timestamp_color_log_console = "app_timestamp_color_log_console";

	/**
	 * getProperty
	 * 
	 * @param propName
	 * @return String
	 */
	public static final String getProperty(String propName) {
		return System.getProperty(propName, "").trim();
	}

	/**
	 * getProperty
	 * 
	 * @param propName
	 * @param defaultValue
	 * @return String
	 */
	public static final String getProperty(String propName, String defaultValue) {
		return System.getProperty(propName, defaultValue).trim();
	}

	/**
	 * getIntProperty
	 * 
	 * @param propName
	 * @return Integer
	 */
	public static final Integer getIntProperty(String propName) {
		return Integer.getInteger(propName, 0);
	}

	/**
	 * getIntProperty
	 * 
	 * @param propName
	 * @param defaultValue
	 * @return Integer
	 */
	public static final Integer getIntProperty(String propName, Integer defaultValue) {
		return Integer.getInteger(propName, defaultValue);
	}

	/**
	 * getColor
	 * 
	 * @param colorProperty
	 * @param defaultColor
	 * @return Color
	 */
	public static final Color getColor(String colorPropertyName, Color defaultColor) {
		if ("".equals(Constants.getProperty(colorPropertyName, ""))) {
			return defaultColor;
		}

		try {
			Color returnColor = Color.decode(Constants.getProperty(colorPropertyName));
			return returnColor;
		} catch (Exception e) {
			LOG.info("Unable to initialize color '" + colorPropertyName + "' due to the error: " + e.getMessage());
			return defaultColor;
		}
	}

}
