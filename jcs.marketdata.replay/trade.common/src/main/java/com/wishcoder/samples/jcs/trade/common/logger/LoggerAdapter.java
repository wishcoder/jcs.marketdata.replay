/**
 * 
 */
package com.wishcoder.samples.jcs.trade.common.logger;

import org.apache.log4j.Level;

/**
 * @author ajaysingh
 *
 */
public final class LoggerAdapter {
	private static LoggerAdapter INSTANCE;
	private ILogger loggerImpl;
	
	private LoggerAdapter() {
		
	}
	/**
	 * Singleton
	 * 
	 * @return LoggerAdapter
	 */
	public static final LoggerAdapter getInstance(){
		if(INSTANCE == null){
			INSTANCE = new LoggerAdapter();
		}
		return INSTANCE; 
	}

	/**
	 * initInstance
	 * @param loggerImpl
	 */
	public void initInstance(ILogger loggerImpl){
		if(this.loggerImpl == null){
			this.loggerImpl = loggerImpl;
		}
	}
	
	/**
	 * insertLog
	 * 
	 * @param logLevel
	 * @param logMessage
	 * @param timeStamp
	 */
	public void insertLog(final Level logLevel, final String logMessage, final long timeStamp){
		if(this.loggerImpl != null){
			this.loggerImpl.insertLog(logLevel, logMessage, timeStamp);
		}
	}
	
}
