package com.wishcoder.samples.jcs.trade.common.logger;

import org.apache.log4j.DailyRollingFileAppender;
import org.apache.log4j.spi.LoggingEvent;


/**
 * @author ajaysingh
 *
 */
public class CustomDailyRollingFileAppender extends DailyRollingFileAppender {

    /**
	 * 
	 */
	public CustomDailyRollingFileAppender() {
		super();
	}

	@Override
    protected void subAppend(LoggingEvent event) {
        LoggingEvent modifiedEvent = new LoggingEvent(event.getFQNOfLoggerClass(), event.getLogger(), event.getTimeStamp(), event.getLevel(), event.getMessage(),
                                                      event.getThreadName(), event.getThrowableInformation(), event.getNDC(), event.getLocationInformation(),
                                                      event.getProperties());
        super.subAppend(modifiedEvent);
        
        LoggerAdapter.getInstance().insertLog(event.getLevel(), "" + event.getMessage(), event.getTimeStamp());
    }
}