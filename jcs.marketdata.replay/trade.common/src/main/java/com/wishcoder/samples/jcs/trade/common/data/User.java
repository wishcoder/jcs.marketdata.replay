/**
 * 
 */
package com.wishcoder.samples.jcs.trade.common.data;

import java.util.List;

/**
 * @author ajaysingh
 *
 */
public class User  {
	public static final int COL_USER_ID = 0;
	public static final int COL_USER_OFFLINE = 1;
	public static final int COL_USER_NAME = 2;
	public static final int COL_USER_SECURITIES = 3;
	public static final int COL_COUNT = 4;
	
	private final String userId;
	private final boolean offline;
	private final String name;
	private final List<CurrencyPair> currencyPairs;
	
	/**
	 * @param userId
	 * @param offline
	 * @param name
	 * @param currencyPairs
	 */
	public User(String userId, boolean offline, String name, List<CurrencyPair> currencyPairs) {
		super();
		this.userId = userId;
		this.offline = offline;
		this.name = name;
		this.currencyPairs = currencyPairs;
	}
	
	/**
	 * @return the offline
	 */
	public boolean isOffline() {
		return offline;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the currencyPairs
	 */
	public List<CurrencyPair> getCurrencyPairs() {
		return currencyPairs;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}
	
	/**
	 * getColumnLabelByIndex
	 * 
	 * @param colIndex
	 * @return label String
	 */
	public static final String getColumnLabelByIndex(int colIndex) {

		switch (colIndex) {

		case COL_USER_ID:
			return "User ID";
		case COL_USER_OFFLINE:
			return "Offline Mode";	
		case COL_USER_NAME:
			return "Name";
		case COL_USER_SECURITIES:
			return "Securities";			
		default:
			return "";
		}
	}

	/**
	 * 
	 * getColumnValueByIndex
	 * 
	 * @param colIndex
	 * @return column value
	 */
	public final String getColumnValueByIndex(int colIndex) {

		switch (colIndex) {

		case COL_USER_ID:
			return getUserId();
		case COL_USER_OFFLINE:
			return isOffline() ? "TRUE" : "FALSE";			
		case COL_USER_NAME:
			return getName();
		case COL_USER_SECURITIES:
			return toScuritiesString();		
		default:
			return "";
		}
	}

	/**
	 * toScuritiesString
	 * @return Securities string
	 */
	private String toScuritiesString() {
		final int maxLen = 100;
		StringBuilder builder = new StringBuilder();
		builder.append(getCurrencyPairs() != null
				? getCurrencyPairs().subList(0, Math.min(getCurrencyPairs().size(), maxLen)) : null);
		return builder.toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "User [userId=" + userId + ", offline=" + offline + ", name=" + name + ", currencyPairs=" + currencyPairs
				+ "]";
	}
}
