package com.wishcoder.samples.jcs.trade.common.data;

public enum CurrencyPair {
	AUD_CAD("AUD/CAD"), AUD_USD("AUD/USD"), EUR_AUD("EUR/AUD"), EUR_CAD("EUR/CAD"), EUR_GBP("EUR/GBP"), USD_CAD(
			"USD/CAD"), USD_JPY("USD/JPY"), CAD_JPY("CAD/JPY"), CHF_RON(
					"CHF/RON"), EUR_HUF("EUR/HUF"), EUR_NZD("EUR/NZD"), GBP_BGN("GBP/BGN"), HKD_JPY("HKD/JPY");

	private final String value;

	CurrencyPair(String value) {
		this.value = value;
	}

	public int getId() {
		switch (this.value) {
		case "AUD/CAD":
			return 1;
		case "AUD/USD":
			return 2;
		case "EUR/AUD":
			return 3;
		case "EUR/CAD":
			return 4;
		case "EUR/GBP":
			return 5;
		case "CAD/JPY":
			return 6;
		case "CHF/RON":
			return 7;
		case "EUR/HUF":
			return 8;
		case "EUR/NZD":
			return 9;
		case "GBP/BGN":
			return 10;
		case "HKD/JPY":
			return 11;
		case "USD/CAD":
			return 12;
		case "USD/JPY":
			return 13;
		}
		return 0;
	}

	@Override
	public String toString() {
		return this.value;
	}
}
