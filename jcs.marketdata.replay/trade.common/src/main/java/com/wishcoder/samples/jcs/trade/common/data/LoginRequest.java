/**
 * 
 */
package com.wishcoder.samples.jcs.trade.common.data;

import java.io.Serializable;

/**
 * @author ajaysingh
 *
 */
public class LoginRequest implements Serializable {
	private static final long serialVersionUID = -1543207576693786717L;

	private final String userId;
	private final String userPwd;
	private final boolean offline;

	/**
	 * @param userId
	 * @param userPwd
	 * @param offline
	 */
	public LoginRequest(String userId, String userPwd, boolean offline) {
		this.userId = userId;
		this.userPwd = userPwd;
		this.offline = offline;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @return the userPwd
	 */
	public String getUserPwd() {
		return userPwd;
	}
	
	/**
	 * @return the offline
	 */
	public boolean isOffline() {
		return offline;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "LoginRequest [userId=" + userId + ", userPwd=" + userPwd + ", offline=" + offline + "]";
	}
}
