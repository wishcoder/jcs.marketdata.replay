/**
 * 
 */
package com.wishcoder.samples.jcs.trade.common.cache;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.jcs.JCS;
import org.apache.jcs.access.CacheAccess;
import org.apache.jcs.access.exception.CacheException;
import org.apache.jcs.engine.control.CompositeCacheManager;
import org.apache.jcs.engine.stats.behavior.ICacheStats;
import org.apache.jcs.engine.stats.behavior.IStatElement;
import org.apache.jcs.engine.stats.behavior.IStats;
import org.slf4j.Logger;

/**
 * @author ajaysingh
 *
 */
class DiskCacheHelper {
	private final Map<String, CacheAccess> cachedObjects; // <cacheName, cache>

	/**
	 * DiskCacheHelper
	 */
	public DiskCacheHelper() {
		this.cachedObjects = new HashMap<String, CacheAccess>();
	}

	/**
	 * @return the cachedObjects
	 */
	public Map<String, CacheAccess> getCachedObjects() {
		return cachedObjects;
	}

	/**
	 * getDataCache
	 * 
	 * @param cacheName
	 * @param cacheLocation
	 * @param maxObjectInMemory
	 * @return CacheAccess
	 * @throws Exception
	 */
	public CacheAccess getDataCache(String cacheName, String cacheLocation, int maxObjectInMemory) throws Exception {
		try {
			CacheAccess cCache = getCachedObjects().get(cacheName);
			if (cCache == null) {
				CompositeCacheManager ccm = CompositeCacheManager.getUnconfiguredInstance();
				ccm.configure(getCacheProperties(cacheName, cacheLocation, maxObjectInMemory));
				cCache = JCS.getInstance(cacheName);
				if (cCache == null) {
					throw new Exception("Unable to create disk cache for '" + cacheName + "'");
				} else {
					getCachedObjects().put(cacheName, cCache);
					return cCache;
				}
			} else {
				return cCache;
			}
		} catch (Exception e) {
			throw new Exception("Unable to create disk cache for '" + cacheName + "' due to exception - " + e);
		}
	}

	/**
	 * writeToCache
	 * 
	 * @param cacheName
	 * @param cacheSeq
	 * @param cacheObject
	 * @throws CacheException
	 */
	public void writeToCache(String cacheName, long cacheSeq, Object[] cacheObject) throws Exception {
		CacheAccess cCache = getCachedObjects().get(cacheName);
		if (cCache != null) {
			cCache.put(cacheSeq, cacheObject);
		} else {
			throw new Exception("Cache '" + cacheName + "' does not exist.");
		}
	}

	/**
	 * shutdownCache
	 * 
	 * @param LOG
	 */
	public void shutdownCache(Logger LOG) {
		for (Map.Entry<String, CacheAccess> cacheEntry : getCachedObjects().entrySet()) {
			CacheAccess cCache = cacheEntry.getValue();
			if (cCache != null) {
				cCache.dispose();
				LOG.info("Cache '" + cacheEntry.getKey() + "' persisted " + getKeyMapSize(cacheEntry.getKey())
						+ " elements.");
			}
		}

		getCachedObjects().clear();
	}

	/**
	 * Get region startup size
	 * 
	 * @param cacheRegion
	 * @return
	 */
	public long getStartupSize(String cacheName) {
		Object objCount = null;
		String startupSizeKey = "Startup Size";
		CacheAccess cCache = getCachedObjects().get(cacheName);
		if (cCache != null) {
			objCount = getCacheConfigurationValue(cCache, startupSizeKey);
		} else {
			return 0;
		}
		if (objCount == null) {
			return 0;
		} else {
			try {
				return Long.parseLong(objCount + "");
			} catch (NumberFormatException ne) {
				return 0;
			}
		}
	}

	/**
	 * Get region key map size
	 * 
	 * @param cacheRegion
	 * @return
	 */
	public long getKeyMapSize(String cacheName) {
		Object objCount = null;
		String keyMapSizeKey = "Key Map Size";
		CacheAccess cCache = getCachedObjects().get(cacheName);
		if (cCache != null) {
			objCount = getCacheConfigurationValue(cCache, keyMapSizeKey);
		} else {
			return 0;
		}
		if (objCount == null) {
			return 0;
		} else {
			try {
				return Long.parseLong(objCount + "");
			} catch (NumberFormatException ne) {
				return 0;
			}
		}
	}

	/**
	 * getCacheConfigurationValue
	 * 
	 * @param cache
	 * @param key
	 * @return Configuration value
	 */
	private Object getCacheConfigurationValue(CacheAccess cache, String key) {
		ICacheStats cacheStats = cache.getStatistics();
		IStats[] arrayAuxiliaryCacheStats = cacheStats.getAuxiliaryCacheStats();
		for (IStats auxiliaryCacheStats : arrayAuxiliaryCacheStats) {
			if ("Indexed Disk Cache".equalsIgnoreCase(auxiliaryCacheStats.getTypeName())) {
				IStatElement[] arrayAuxiliaryCacheStatsElements = auxiliaryCacheStats.getStatElements();
				for (IStatElement auxiliaryCacheStatsElement : arrayAuxiliaryCacheStatsElements) {
					if (key.equalsIgnoreCase(auxiliaryCacheStatsElement.getName())) {
						return auxiliaryCacheStatsElement.getData();
					}
				}
			}
		}
		return null;
	}

	/**
	 * getCacheProperties
	 * 
	 * @param cacheName
	 * @param cacheLocation
	 * @param maxObjectInMemory
	 * @return Properties
	 */
	private Properties getCacheProperties(String cacheName, String cacheLocation, int maxObjectInMemory) {
		Properties props = new Properties();
		// # DEFAULT CACHE REGION
		// # sets the default aux value for any non configured caches
		props.put("jcs.default", "DC");
		props.put("jcs.default.cacheattributes", "org.apache.jcs.engine.CompositeCacheAttributes");
		props.put("jcs.default.cacheattributes.MaxObjects", maxObjectInMemory);
		props.put("jcs.default.cacheattributes.MemoryCacheName", "org.apache.jcs.engine.memory.lru.LRUMemoryCache");
		props.put("jcs.default.cacheattributes.DiskUsagePatternName", "UPDATE");
		props.put("jcs.default.elementattributes", "org.apache.jcs.engine.ElementAttributes");
		props.put("jcs.default.elementattributes.IsSpool", "true");
		props.put("jcs.region.elementattributes.IsEternal", "false");
		props.put("jcs.default.elementattributes.IsLateral", "false");
		props.put("jcs.default.elementattributes.IsRemote", "false");

		// #Configurations for auxilary/disk cache
		props.put("jcs.auxiliary.DC", "org.apache.jcs.auxiliary.disk.indexed.IndexedDiskCacheFactory");
		props.put("jcs.auxiliary.DC.attributes", "org.apache.jcs.auxiliary.disk.indexed.IndexedDiskCacheAttributes");
		props.put("jcs.auxiliary.DC.attributes.DiskPath", cacheLocation);

		/*
		 * Writing to the disk cache is asynchronous and made efficient by using
		 * a memory staging area called purgatory. Retrievals check purgatory
		 * then disk for an item. When items are sent to purgatory they are
		 * simultaneously queued to be put to disk. If an item is retrieved from
		 * purgatory it will no longer be written to disk, since the cache hub
		 * will move it back to memory. Using purgatory insures that there is no
		 * wait for disk writes, unnecessary disk writes are avoided for
		 * borderline items, and the items are always available.
		 * 
		 */
		props.put("jcs.auxiliary.DC.attributes.MaxPurgatorySize", "10000000"); // 10000000

		// If the maximum key size is less than 0, no limit will be placed on
		// the number of keys. By default, the max key size is 5000.
		props.put("jcs.auxiliary.DC.attributes.MaxKeySize", "-1");
		props.put("jcs.auxiliary.DC.attributes.EventQueueType", "POOLED");
		props.put("jcs.auxiliary.DC.attributes.EventQueuePoolName", "disk_cache_event_queue");

		// # Disk Cache Event Queue Pool
		props.put("thread_pool.disk_cache_event_queue.useBoundary", "false");
		props.put("thread_pool.remote_cache_client.maximumPoolSize", "15");
		props.put("thread_pool.disk_cache_event_queue.minimumPoolSize", "1");
		props.put("thread_pool.disk_cache_event_queue.keepAliveTime", "3500");
		props.put("thread_pool.disk_cache_event_queue.startUpSize", "1");

		// #Configuration for DATA CACHE REGION
		props.put("jcs.region." + cacheName, "DC");
		props.put("jcs.region." + cacheName + ".cacheattributes", "org.apache.jcs.engine.CompositeCacheAttributes");
		props.put("jcs.region." + cacheName + ".cacheattributes.MaxObjects", maxObjectInMemory);
		props.put("jcs.region." + cacheName + ".cacheattributes.MemoryCacheName",
				"org.apache.jcs.engine.memory.lru.LRUMemoryCache");
		props.put("jcs.region." + cacheName + ".cacheattributes.DiskUsagePatternName", "UPDATE");
		props.put("jcs.region." + cacheName + ".elementattributes", "org.apache.jcs.engine.ElementAttributes");
		props.put("jcs.region." + cacheName + ".elementattributes.IsEternal", "false");
		props.put("jcs.region." + cacheName + ".elementattributes.IsLateral", "false");
		props.put("jcs.region." + cacheName + ".elementattributes.IsRemote", "false");

		return props;
	}
}
