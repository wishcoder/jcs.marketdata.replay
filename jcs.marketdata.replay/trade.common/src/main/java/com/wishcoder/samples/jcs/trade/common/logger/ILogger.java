/**
 * 
 */
package com.wishcoder.samples.jcs.trade.common.logger;

import org.apache.log4j.Level;

/**
 * @author ajaysingh
 *
 */
public interface ILogger {
	public void insertLog(final Level logLevel, final String logMessage, final long timeStamp);
}