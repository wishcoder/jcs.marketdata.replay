/**
 * 
 */
package com.wishcoder.samples.jcs.trade.common.data;


/**
 * @author ajaysingh
 *
 */
public class TickEvent extends AbstractTickEvent {
	private static final long serialVersionUID = 8343089189490124797L;
	private final Tick tick;
	
	/**
	 * @param arg0
	 */
	public TickEvent(User user, TickBook tickBook, Tick tick) {
		super(user, tickBook);
		this.tick = tick;
	}

	/**
	 * @return the tick
	 */
	public Tick getTick() {
		return tick;
	}
}
