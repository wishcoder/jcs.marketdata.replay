/**
 * 
 */
package com.wishcoder.samples.jcs.trade.common.data;

import java.io.Serializable;
import java.text.DecimalFormat;

/**
 * @author ajaysingh
 *
 */
public class Tick implements Serializable {
	private static DecimalFormat df2 = new DecimalFormat(".##");
	
	public static final int COL_SECURITY = 0;
	public static final int COL_BID = 1;
	public static final int COL_ASK = 2;
	public static final int COL_COUNT = 3;

	private static final long serialVersionUID = 8703936553586209468L;
	private final Security security;
	private final double bid;
	private final double ask;

	/**
	 * @param security
	 * @param bid
	 * @param ask
	 */
	public Tick(Security security, double bid, double ask) {
		super();
		this.security = security;
		this.bid = bid;
		this.ask = ask;
	}

	/**
	 * getColumnLabelByIndex
	 * 
	 * @param colIndex
	 * @return label String
	 */
	public static final String getColumnLabelByIndex(int colIndex) {

		switch (colIndex) {

		case COL_SECURITY:
			return "Security";
		case COL_BID:
			return "Bid";
		case COL_ASK:
			return "Ask";			
		default:
			return "";
		}
	}

	/**
	 * 
	 * getColumnValueByIndex
	 * 
	 * @param colIndex
	 * @return column value
	 */
	public final String getColumnValueByIndex(int colIndex) {

		switch (colIndex) {

		case COL_SECURITY:
			return getSecurity().getCurrencyPair().toString();
		case COL_BID:
			if(getBid() == 0.0D){
				return "";
			}
			return df2.format(getBid());
		case COL_ASK:
			if(getAsk() == 0.0D){
				return "";
			}
			return df2.format(getAsk());			
		default:
			return "";
		}

	}

	/**
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * @return the bid
	 */
	public double getBid() {
		return bid;
	}

	/**
	 * @return the ask
	 */
	public double getAsk() {
		return ask;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Tick [security=" + security + ", bid=" + bid + ", ask=" + ask + "]";
	}
}
