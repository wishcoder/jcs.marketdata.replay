/**
 * 
 */
package com.wishcoder.samples.jcs.trade.common.data;

import java.io.Serializable;
import java.util.List;

/**
 * @author ajaysingh
 *
 */
public class LoginResponse implements Serializable {
	private static final long serialVersionUID = -1970443226611971271L;
	private final String userId;
	private final String name;
	private final List<CurrencyPair> currencyPairs;

	/**
	 * @param userId
	 * @param name
	 * @param currencyPairs
	 */
	public LoginResponse(String userId, String name, List<CurrencyPair> currencyPairs) {
		super();
		this.userId = userId;
		this.name = name;
		this.currencyPairs = currencyPairs;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the currencyPairs
	 */
	public List<CurrencyPair> getCurrencyPairs() {
		return currencyPairs;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "LoginResponse [userId=" + userId + ", name=" + name + ", currencyPairs=" + currencyPairs + "]";
	}
}
