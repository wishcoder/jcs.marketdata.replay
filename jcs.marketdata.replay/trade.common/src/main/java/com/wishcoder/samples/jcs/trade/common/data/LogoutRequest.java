/**
 * 
 */
package com.wishcoder.samples.jcs.trade.common.data;

import java.io.Serializable;

/**
 * @author ajaysingh
 *
 */
public class LogoutRequest implements Serializable {
	private static final long serialVersionUID = -1637548175243883995L;

	private final String userId;

	/**
	 * LogoutRequest
	 * 
	 * @param userId
	 */
	public LogoutRequest(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "LogoutRequest [userId=" + userId + "]";
	}
}
