/**
 * 
 */
package com.wishcoder.samples.jcs.trade.common.data;

import java.io.Serializable;

/**
 * @author ajaysingh
 *
 */
public class Security implements Serializable {
	private static final long serialVersionUID = -706562426647198674L;

	private final int id;
	private final CurrencyPair currencyPair;
	
	/**
	 * @param id
	 * @param currencyPair
	 */
	public Security(int id, CurrencyPair currencyPair) {
		super();
		this.id = id;
		this.currencyPair = currencyPair;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the currencyPair
	 */
	public CurrencyPair getCurrencyPair() {
		return currencyPair;
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Security [id=" + id + ", currencyPair=" + currencyPair.toString() + "]";
	}

}
