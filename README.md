# README #

### What is this repository for? ###

* Java application to demonstrate how to persist application runtime data to disk store and replay data from disk store using Apache Java Caching System (JCS) and Javassist.

* Version 1.0

### How do I get set up? ###
 
* View [JCS Marketdata Persistence and Replay](https://bitbucket.org/wishcoder/jcs.marketdata.replay/wiki/Home) wiki for more details

### Legal information ###

The library is licensed under the  LGPLv3.0 - see [LICENSE.txt](https://bitbucket.org/wishcoder/jcs.marketdata.replay/raw/a3393ba7c720cdbb17411055dc54c2aff94dbbb7/jcs.marketdata.replay/LICENSE.txt)

### Who do I talk to? ###
* Ajay Singh [message4ajay@gmail.com]